from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.forms import LoginUser, SignupUser


# Create your views here.
def login_user(request):  # Feature 7: Login user
    if request.method == "POST":
        loginform = LoginUser(request.POST)
        if loginform.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )

            if user is not None:
                login(request, user)
                return redirect("home")  # Redirects to "projects/"
            else:
                loginform.add_error("password", "Passwords do not match")

    else:
        loginform = (
            LoginUser()
        )  # Note for self: Try with request.GET for tests

    context = {
        "user": loginform,
    }

    return render(request, "accounts/login.html", context)


def logout_user(request):  # Feature 9, logs out the user and redirects
    logout(request)
    return redirect("login")


def signup_user(request):  # Feature 10, creates a new user and redirects
    if request.method == "POST":
        form = SignupUser(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            # Creates new user if passwords match
            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                )
                login(request, user)
                return redirect("list_projects")  # Redirects to "projects/"
            else:  # Returns a form error if the passwords do not match
                form.add_error("password", "the password do not match")
    else:
        form = SignupUser()  # Note for self - Try with request.GET for tests
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
