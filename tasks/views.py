from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTask
from tasks.models import Task


# Create new task view for Feature 15
@login_required(redirect_field_name="/accounts/login")
def create_task(request):
    if request.method == "POST":
        form = CreateTask(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTask()
    context = {"form": form}

    return render(request, "tasks/create.html", context)


# View my tasks for Feature 16
@login_required(redirect_field_name="/accounts/login")
def show_my_tasks(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {"task_list": task_list}
    return render(request, "tasks/mine.html", context)
