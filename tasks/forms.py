from django.forms import ModelForm
from tasks.models import Task


# Task form for Feature 15
class CreateTask(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
