from django.contrib import admin
from projects.models import Project


# Register your models here.
@admin.register(Project)  # Feature 4; adds Project to the admin site
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "owner",
    )
