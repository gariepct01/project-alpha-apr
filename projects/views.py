from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import CreateProject


# Create your views here.
# Feature 5; show all instances of the Project model
@login_required(redirect_field_name="/accounts/login")  # Added with Feature 8
def list_projects(request):
    project_list = Project.objects.filter(
        owner=request.user
    )  # Adjusted for Feature 8 to show projects *only* belonging to User
    context = {"project_list": project_list}
    return render(request, "projects/projects.html", context)


# Added with Feature 13; shows tasks for a specific project
@login_required(redirect_field_name="/accounts/login")
def show_project(request, id):
    project = get_object_or_404(Project, id=id)  # Grabs the project with ID
    context = {"project": project}
    return render(request, "projects/detail.html", context)


# Added with Feature 14; create new projects
@login_required(redirect_field_name="/accounts/login")
def create_project(request):
    if request.method == "POST":
        form = CreateProject(request.POST)
        if form.is_valid():  # Creates new project if valid
            new_project = form.save(False)
            new_project.owner = request.user
            new_project = form.save()
            return redirect("list_projects")
    else:
        form = CreateProject()

    context = {"form": form}
    return render(request, "projects/create.html", context)
